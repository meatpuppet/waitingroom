/*
Copyright © 2023 NAME HERE <EMAIL ADDRESS>
*/
package cmd

import (
	// "encoding/json"
	"fmt"
	"github.com/pelletier/go-toml/v2"
	"codeberg.org/meatpuppet/waitingroom/config"
	"github.com/spf13/cobra"
)

// configCmd represents the config command
var configCmd = &cobra.Command{
	Use:   "config",
	Short: "print the current config",
	Long: `print the current config, json formatted`,
	Run: func(cmd *cobra.Command, args []string) {
        // j, err := json.MarshalIndent(config.Config, "", "  ")
		j, err := toml.Marshal(config.Config)
        if err != nil {
            panic(err)
        }
        fmt.Println(string(j))
	},
}

func init() {
	rootCmd.AddCommand(configCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// configCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// configCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
