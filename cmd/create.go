/*
Copyright © 2023 NAME HERE <EMAIL ADDRESS>
*/
package cmd

import (
	"fmt"

	"codeberg.org/meatpuppet/waitingroom/config"
	"github.com/mitchellh/mapstructure"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

// createCmd represents the create command
var createCmd = &cobra.Command{
	Use:   "create",
	Short: "write the default config to file (wont overwrite existing files)",
	Long: "",
    Run: func(cmd *cobra.Command, args []string) {
		items := map[string]interface{}{}
		if err := mapstructure.Decode(config.Config, &items); err != nil {
			fmt.Println(err)
		}

		if err := viper.MergeConfigMap(items); err != nil {
			fmt.Println(err)
		}

		err := viper.SafeWriteConfig()
		if err != nil {
			fmt.Println(err)
			return
		}

		if err := viper.ReadInConfig(); err == nil {
			fmt.Printf("written config file to %s\n", viper.ConfigFileUsed())
		}
	},
}

func init() {
	configCmd.AddCommand(createCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// createCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// createCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
