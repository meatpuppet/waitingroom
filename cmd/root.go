/*
Copyright © 2023 NAME HERE <EMAIL ADDRESS>
*/
package cmd

import (
	"codeberg.org/meatpuppet/waitingroom/mpdclient"
	"fmt"
	"os"

	c "codeberg.org/meatpuppet/waitingroom/config"
	"codeberg.org/meatpuppet/waitingroom/waitingroom"
	"github.com/rs/zerolog"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"

	tea "github.com/charmbracelet/bubbletea"
	"github.com/charmbracelet/lipgloss"
)

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "waitingroom",
	Short: "a tui mpd client",
	PersistentPreRunE: func(cmd *cobra.Command, args []string) error {
		// You can bind cobra and viper in a few locations, but PersistencePreRunE on the root command works well
		// Set the base name of the config file, without the file extension.
		viper.SetConfigName(defaultConfigFilename)
		viper.SetConfigType("toml")

		// Set as many paths as you like where viperiper should look for the
		// config file. We are only looking in the current working directory.
		viper.AddConfigPath("$HOME/.config/waitingroom")
		return initializeConfig(cmd)
	},
	// Uncomment the following line if your bare application
	// has an action associated with it:
	Run: func(cmd *cobra.Command, args []string) {
		run()
	},
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}

func init() {
	// Here you will define your flags and configuration settings.
	// Cobra supports persistent flags, which, if defined here,
	// will be global for your application.

	// rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.waitingroom.yaml)")

	// Cobra also supports local flags, which will only run
	// when this action is called directly.
}

func initStyles() {
	c.Styles.PassiveItemStyle = lipgloss.NewStyle()
	c.Styles.CursorItemStyle = lipgloss.NewStyle().
		Background(lipgloss.Color(c.Config.Colors.CursorBackground)).
		Foreground(lipgloss.Color(c.Config.Colors.CursorForeground))
	c.Styles.InactiveCursorItemStyle = lipgloss.NewStyle().
		Background(lipgloss.Color(c.Config.Colors.InactiveCursorBackground)).
		Foreground(lipgloss.Color(c.Config.Colors.InactiveCursorForeground))
	c.Styles.AlternatePassiveStyle = lipgloss.NewStyle().
		Foreground(lipgloss.Color(c.Config.Colors.ForegroundAlternate))
	c.Styles.ActiveItemStyle = lipgloss.NewStyle().
		Bold(true)
	c.Styles.HeadlineStyle = lipgloss.NewStyle().
		Bold(true)

	c.Styles.BorderBase = lipgloss.NewStyle().
		Padding(0, 2).
		Border(
			lipgloss.RoundedBorder())
	c.Styles.ActiveBorder = c.Styles.BorderBase.
		BorderForeground(lipgloss.Color(c.Config.Colors.ActiveBorder))
	c.Styles.PassiveBorder = c.Styles.BorderBase.
		BorderForeground(lipgloss.Color(c.Config.Colors.PassiveBorder))
}

func run() {
	// setup logging
	logLevel, err := zerolog.ParseLevel(c.Config.LogLevel)
	if err != nil {
		panic(err)
	}
	zerolog.SetGlobalLevel(logLevel)

	if c.Config.LogPath != "" {
		file, err := os.OpenFile(
			c.Config.LogPath,
			os.O_APPEND|os.O_CREATE|os.O_WRONLY,
			0664,
		)
		if err != nil {
			panic(err)
		}
		c.Logger = zerolog.New(zerolog.ConsoleWriter{Out: file}).With().Timestamp().Logger()
		defer file.Close()
	}
	c.Logger.Info().Msgf("loglevel: %+v", c.Config.LogLevel)

	initStyles()

	// start waitingroom!
	m := waitingroom.InitialModel(c.Config)
	p := tea.NewProgram(
		m,
		tea.WithAltScreen(),       // use the full size of the terminal in its "alternate screen buffer"
		tea.WithMouseCellMotion(), // turn on mouse support so we can track the mouse wheel
	)

	defer m.MpdClient.Close()
	// defer p.ReleaseTerminal()

	go func() {
		for {
			c.Logger.Info().Msg("starting watcher loop")
			for err := range m.MpdClient.Watcher.Error {
				c.Logger.Err(err)
			}
			c.Logger.Info().Msg("ending watcher err loop")
		}
	}()
	// get events
	go func() {
		for {
			c.Logger.Info().Msg("starting event loop")
			for subsystem := range m.MpdClient.Watcher.Event {
				c.Logger.Info().Msg(fmt.Sprintf("got update in %s", subsystem))
				p.Send(mpdclient.GetUpdateMsg(subsystem))
			}
			c.Logger.Info().Msg("ending event loop")
		}
	}()

	if _, err := p.Run(); err != nil {
		os.Exit(1)
	}
}
