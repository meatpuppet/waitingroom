package cmd

import (
	"strings"

	"codeberg.org/meatpuppet/waitingroom/config"
	"github.com/mcuadros/go-defaults"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

const defaultConfigFilename = "config"
const envPrefix = "WR"
const replaceHyphenWithCamelCase = true

// https://github.com/carolynvs/stingoftheviper/blob/main/main.go
func initializeConfig(_ *cobra.Command) error {
    defaults.SetDefaults(&config.Config)

	// Attempt to read the config file, gracefully ignoring errors
	// caused by a config file not being found. Return an error
	// if we cannot parse the config file.
	if err := viper.ReadInConfig(); err != nil {
		//log.Println(err)
		// It's okay if there isn't a config file
		if _, ok := err.(viper.ConfigFileNotFoundError); !ok {
            return err
		}
	}

	// When we bind flags to environment variables expect that the
	// environment variables are prefixed, e.g. a flag like --number
	// binds to an environment variable STING_NUMBER. This helps
	// avoid conflicts.
	viper.SetEnvPrefix(envPrefix)

	// Environment variables can't have dashes in them, so bind them to their equivalent
	// keys with underscores, e.g. --favorite-color to STING_FAVORITE_COLOR
	viper.SetEnvKeyReplacer(strings.NewReplacer("-", "_"))

	// Bind to environment variables
	// Works great for simple config names, but needs help for names
	// like --favorite-color which we fix in the bindFlags function
	viper.AutomaticEnv()
	viper.BindEnv("mpd_host", "MPD_HOST")
	viper.BindEnv("mpd_port", "MPD_PORT")

	viper.Unmarshal(&config.Config)
	return nil
}
