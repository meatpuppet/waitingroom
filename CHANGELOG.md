# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [Unreleased]

### Added

- playlist marking / append to marked playlist
- add command mode: save, search, sort

### Changed

### Deprecated

### Removed

### Fixed

- config output as toml
- fix losing path after library update

### Security


## [0.0.8]

### Added

- show error message when playlist saving fails
- add volume controls

### Changed

- update dependencies

### Fixed

- fix broken statusbar when text has symbols
- dont jump down when deleting from queue


## [0.0.7]

### Changed

- update requirements
- restore original terminal state after exit
- change style handling for lipgloss 0.11, since styles are immutable now


## [0.0.6]

### Fixed

- only paste if clipboard is not empty


## [0.0.5]

### Added

- started the changelog
- added fast forward/rewind feature
- delete/yank in queue will add item to "clipboard"
- paste from clipboard

### Fixed

- make UpdateLibrary a global key (was library-only before)

