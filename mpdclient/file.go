package mpdclient

import (
	"fmt"
	"strconv"
	"strings"
	"time"

	"codeberg.org/meatpuppet/waitingroom/config"
	"github.com/fhs/gompd/v2/mpd"
)

type fileType int

const (
	DirectoryType fileType = iota
	FileType
	PlaylistType
)

type File struct {
	FileType     fileType
	Path         string
	LastModified time.Time
	displayText  string

	id int

	Title    string
	Artist   string
	Duration int

	Album       string
	AlbumArtist string
	TrackNr     int

	IsMarked bool
}

func (t File) GetPath() string {
	return t.Path
}

func (t File) GetLastModified() time.Time {
	return t.LastModified
}

func (t File) Text() string {
	switch t.FileType {
	case DirectoryType:
		split := strings.Split(t.Path, "/")
		return split[len(split)-1] + "/"
	case PlaylistType:
		return t.Path
	default:
		if t.Artist == t.AlbumArtist {
			return fmt.Sprintf("%02d [%s] %s", t.TrackNr, HumanizeTime(t.Duration), t.Title)
		} else {
			return fmt.Sprintf("%02d [%s] %s - %s", t.TrackNr, HumanizeTime(t.Duration), t.Artist, t.Title)
		}
	}
}

func (t File) TextRight() string {
	if t.IsMarked {
		return "m"
	}
	return ""
}

func (t File) Id() int {
	return t.id
}

func (t File) Group() string {
	split := strings.Split(t.Path, "/")
	group := strings.Join(split[:len(split)-1], "/")
	group += "/"
	return group
}

func (t File) Length() int {
	return t.Duration
}

func FilesFromAttrs(infos []mpd.Attrs) []File {
	files := make([]File, 0)
	for _, info := range infos {
		files = append(files, FileFromAttrs(info))
	}
	return files
}

func PlaylistFilesFromAttrs(infos []mpd.Attrs) []File {
	files := make([]File, 0)
	for _, info := range infos {
		files = append(files, PlaylistFileFromAttrs(info))
	}
	return files
}

func (t File) getFileDisplayText() string {
	return fmt.Sprintf("[%s] %s - %s", HumanizeTime(t.Duration), t.Artist, t.Title)
}

func PlaylistFileFromAttrs(info mpd.Attrs) File {
	t := File{}
	t.Path = info["file"]
	t.Artist = info["Artist"]
	time, err := strconv.Atoi(info["Time"])
	if err != nil {
		time = 0
	}
	pos, err := strconv.Atoi(info["Track"])
	if err != nil {
		pos = 0
	}
	t.TrackNr = pos
	t.Duration = time
	t.Title = info["Title"]
	t.Album = info["Album"]
	t.AlbumArtist = info["Albumartist"]

	t.FileType = FileType

	// playlist files dont have ids
	//
	return t
}

func parseTimestamp(s string) (time.Time, error) {
	return time.Parse(time.RFC3339, s)
}

func FileFromAttrs(info mpd.Attrs) File {
	t := File{}
	if info["directory"] != "" {
		// map[
		//	directory:some_dirname..
		//	last-modified:2019-03-23T18:16:19Z
		// ]
		t.Path = info["directory"]
		lastModified, err := parseTimestamp(info["last-modified"])
		if err == nil {
			// config.Logger.Info().Msgf("parsing time: %v", info)
			t.LastModified = lastModified
		} else {
			config.Logger.Error().Msgf("error parsing time: %v, err: %s", info, err)
		}
		t.FileType = DirectoryType
	} else if info["file"] != "" {
		// config.Logger.Info().Msgf("file info %+v", info)
		t.Path = info["file"]
		t.Artist = info["artist"]
		time, err := strconv.Atoi(info["time"])
		if err != nil {
			time = 0
		}
		pos, err := strconv.Atoi(info["track"])
		if err != nil {
			pos = 0
		}
		t.TrackNr = pos
		t.Duration = time
		t.Title = info["title"]
		t.Album = info["album"]
		t.AlbumArtist = info["albumartist"]
		lastModified, err := parseTimestamp(info["last-modified"])
		if err == nil {
			t.LastModified = lastModified
		} else {
			config.Logger.Error().Msgf("error parsing time: %v, err: %s", info, err)
		}
		t.FileType = FileType

	} else if info["playlist"] != "" {
		// if playlists come as part of the library, "last-modified" will be lower-case
		// getting playlists directly, it will be upper case.
		// since we have a seperate call to get the playlists, we use the upper-case version here.
		//
		// playlist only has
		// map[
		//	last-modified:2023-08-04T19:57:58Z
		//	playlist:playlistname
		// ]
		// config.Logger.Info().Msgf("playlist info %+v", info)
		t.Path = info["playlist"]
		lastModified, err := parseTimestamp(info["Last-Modified"])
		// lastModified, err := parseTimestamp(info["last-modified"])
		if err == nil {
			t.LastModified = lastModified
		} else {
			config.Logger.Error().Msgf("error parsing time: %v, err: %s", info, err)
		}
		t.FileType = PlaylistType
	} else {
		config.Logger.Info().Msgf("unknown file info %+v", info)
	}
	return t
}
