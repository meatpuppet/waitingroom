package mpdclient

import (
	"fmt"
	"log"
	"strconv"

	"codeberg.org/meatpuppet/waitingroom/config"
	c "codeberg.org/meatpuppet/waitingroom/config"
	tea "github.com/charmbracelet/bubbletea"
	"github.com/fhs/gompd/v2/mpd"
)

type MpdClient struct {
	*mpd.Client
	*mpd.Watcher
}

type SubsystemEventMsg string

type State int

const (
	PlayingState State = iota
	PausedState
	StoppedState
)

type MpdStatus struct {
	PlaylistLength int
	Song           int //song: playlist song number of the current song stopped on or playing
	Elapsed        int
	State          State
	Volume         int
}

func GetUpdateMsg(subsystem string) tea.Msg {
	msg := SubsystemEventMsg(subsystem)
	return msg
}

func (s State) GetStateSymbol() string {
	switch s {
	case PausedState:
		return config.Config.Symbols.Paused
	case PlayingState:
		return config.Config.Symbols.Playing
	case StoppedState:
		return config.Config.Symbols.Stopped
	}
	return config.Config.Symbols.Stopped
}

func (mpd *MpdClient) Status() MpdStatus {
	//https://mpd.readthedocs.io/en/latest/protocol.html#querying-mpd-s-status
	status, err := mpd.Client.Status()
	if err != nil {
		config.Logger.Error().Err(err)
	}

	playlistLength, err := strconv.Atoi(status["playlistlength"])
	if err != nil {
		config.Logger.Error().Err(err)
	}

	volume, err := strconv.Atoi(status["volume"])
	if err != nil {
		config.Logger.Error().Err(err)
	}

	song, err := strconv.Atoi(status["song"])
	if err != nil {
		config.Logger.Error().Err(err)
	}

	// elapsed is a float string...
	e, err := strconv.ParseFloat(status["elapsed"], 32)
	if err != nil {
		config.Logger.Error().Err(err)
	}
	elapsed := int(e)

	s := status["state"]

	state := StoppedState
	switch s {
	case "play":
		state = PlayingState
	case "paused":
		state = PausedState
	case "stopped":
		state = StoppedState
	}

	return MpdStatus{
		PlaylistLength: playlistLength,
		Song:           song,
		Elapsed:        elapsed,
		State:          state,
		Volume:         volume,
	}
}

func (mpd *MpdClient) CurrentSong() Track {
	attrs, err := mpd.Client.CurrentSong()
	if err != nil {
		config.Logger.Error().Err(err)
	}
	return TrackFromAttrs(attrs)
}

func GetClient(mpdHost string, mpdPort string, mpdPassword string) *MpdClient {
	mpdAddress := fmt.Sprintf("%s:%s", mpdHost, mpdPort)

	c.Logger.Info().Msg(fmt.Sprintf("connecting to %s", mpdAddress))
	client, err := mpd.DialAuthenticated("tcp", mpdAddress, mpdPassword)
	if err != nil {
		log.Printf("cant connect to mpd at %s", mpdAddress)
		c.Logger.Fatal().Msgf("cant connect to mpd at %s", mpdAddress)
	}

	err = client.Ping()
	if err != nil {
		log.Printf("cant ping mpd at %s", mpdAddress)
		c.Logger.Fatal().Msgf("cannot ping mpd at %s", mpdAddress)
	}

	watcher, err := mpd.NewWatcher("tcp", mpdAddress, mpdPassword)
	if err != nil {
		log.Printf("cant create watcher!")
		c.Logger.Fatal().Err(err)
	}
	return &MpdClient{client, watcher}
}

func (mpd *MpdClient) Close() {
	c.Logger.Info().Msg("closing connection")
	mpd.Client.Close()
	mpd.Watcher.Close()
}

func (mpd *MpdClient) GetQueue() []Track {
	playlistInfo, err := mpd.Client.PlaylistInfo(-1, -1)
	if err != nil {
		panic("could not get playlist!")
	}
	return TracksFromAttrs(playlistInfo)
}

/*
*  seek song in pos to sec
 */
func (mpd *MpdClient) Seek(pos, sec int) {
	mpd.Client.Seek(pos, sec)
}

/*
* search the db
*/
func (mpd *MpdClient) Search(term string) ([]Track, error) {
	attrs, err := mpd.Client.Search("(Any contains \"" + term + "\")")
	if err != nil {
		return nil, err
	}
	return TracksFromAttrs(attrs), nil
}
