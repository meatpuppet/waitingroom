package mpdclient

import (
	c "codeberg.org/meatpuppet/waitingroom/config"
)

func (mpd *MpdClient) UpdateLibrary() {
	mpd.Client.Update("/")
}

func (mpd *MpdClient) GetLibraryPath(path string) (int, []File) {
	playlistInfo, err := mpd.ListInfo(path)
	if err != nil {
		c.Logger.Error().Err(err).Msg("could not get library!")
	}
	allFiles := FilesFromAttrs(playlistInfo)
	filesWithoutPlaylists := []File{}
	for _, file := range allFiles {
		if file.FileType != PlaylistType {
			filesWithoutPlaylists = append(filesWithoutPlaylists, file)
		}
	}
	return 0, filesWithoutPlaylists
}
