package mpdclient

import (
	"fmt"
	"strconv"
	"time"

	"github.com/fhs/gompd/v2/mpd"
)

type Track struct {
	Title       string
	Artist      string
	AlbumArtist string
	Duration    int
	LastModified time.Time

	Album   string
	Year    string
	TrackNr int

	Path string

	id int

	Pos int
}

func (t Track) GetPath() string {
	return t.Path
}

func (t Track) GetLastModified() time.Time {
	// todo: LastModified is not set for tracks yet
	// (would allow sorting queue and playlists - do we want that?)
	return t.LastModified
}

func (t Track) Id() int {
	return t.id
}

func HumanizeTime(s int) string {
	minutes := s / 60
	seconds := s % 60
	str := fmt.Sprintf("%d:%02d", minutes, seconds)
	return str
}

func (t Track) Text() string {
	// since albumartist is part of the
	if t.AlbumArtist != t.Artist {
		return fmt.Sprintf("[%s] %s - %s", HumanizeTime(t.Duration), t.Artist, t.Title)
	}
	return fmt.Sprintf("[%s] %s", HumanizeTime(t.Duration), t.Title)
}

func (t Track) TextRight() string {
	return ""
}

func (t Track) Group() string {
	return fmt.Sprintf("%s - %s", t.AlbumArtist, t.Album)
}

func (t Track) Length() int {
	return t.Duration
}

func TracksFromAttrs(infos []mpd.Attrs) []Track {
	tracks := make([]Track, 0)
	for _, info := range infos {
		tracks = append(tracks, TrackFromAttrs(info))
	}
	return tracks
}

func TrackFromAttrs(info mpd.Attrs) Track {
	/*
	   *%s: %s Label Virgin
	   %s: %s Time 498
	   %s: %s duration 497.534
	   %s: %s Last-Modified 2019-03-23T08:54:55Z
	   %s: %s OriginalDate 1998
	   %s: %s MUSICBRAINZ_ALBUMID 367899
	   %s: %s Disc 1
	   %s: %s MUSICBRAINZ_ARTISTID 28970
	   %s: %s MUSICBRAINZ_ALBUMARTISTID 28970
	   %s: %s Pos 37
	   %s: %s file The Smashing Pumpkins/Adore/14 For Martha.mp3
	   %s: %s Title For Martha
	   %s: %s Album Adore
	   %s: %s Date 1998
	   %s: %s Id 467
	   %s: %s Artist The Smashing Pumpkins
	   %s: %s AlbumArtist The Smashing Pumpkins
	   %s: %s Track 14
	   %s: %s MUSICBRAINZ_TRACKID 367899-14
	*/
	t := Track{}
	t.Artist = info["Artist"]
	t.Title = info["Title"]
	t.Album = info["Album"]
	t.AlbumArtist = info["AlbumArtist"]
	t.Year = info["Date"]

	t.Path = info["file"]

	trackNr, err := strconv.Atoi(info["Track"])
	if err != nil {
		trackNr = 0
	}
	t.TrackNr = trackNr

	id, err := strconv.Atoi(info["Id"])
	if err != nil {
		id = 0
	}
	t.id = id

	time, err := strconv.Atoi(info["Time"])
	if err != nil {
		time = 0
	}
	t.Duration = time

	pos, err := strconv.Atoi(info["Pos"])
	if err != nil {
		pos = -1
	}
	t.Pos = pos

	//for k, v := range *t.MpdInfo {
	//	fmt.Println("%s: %s", k, v)
	//}
	return t
}
