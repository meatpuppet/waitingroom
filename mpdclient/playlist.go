package mpdclient

import "codeberg.org/meatpuppet/waitingroom/config"

func (mpd *MpdClient) PlaylistMove(name string, from int, to int) {
	err := mpd.Client.PlaylistMove(name, from, to)
    if err != nil {
        config.Logger.Error().Err(err)
    }
}

func (mpd *MpdClient) PlaylistDelete(name string, pos int) {
	err := mpd.Client.PlaylistDelete(name, pos)
    if err != nil {
        config.Logger.Error().Err(err)
    }
}

func (mpd *MpdClient) PlaylistRemove(name string) {
	err := mpd.Client.PlaylistRemove(name)
    if err != nil {
        config.Logger.Error().Err(err)
    }
}

func (mpd *MpdClient) PlaylistAdd(name, uri string) error {
	err := mpd.Client.PlaylistAdd(name, uri)
    if err != nil {
        config.Logger.Error().Err(err)
    }
	return err
}

func (mpd *MpdClient) PlaylistSave(name string) error {
	err := mpd.Client.PlaylistSave(name)
	if err != nil {
		config.Logger.Error().Err(err).Msgf("could not save playlist %s!", name)
	}
	return err
}

func (mpd *MpdClient) GetPlaylist(path string) []File {
	playlistInfo, err := mpd.Client.PlaylistContents(path)
	if err != nil {
		config.Logger.Error().Err(err).Msg("could not get playlist!")
	}
	return PlaylistFilesFromAttrs(playlistInfo)
}

func (mpd *MpdClient) GetPlaylists() []File {
	playlistInfo, err := mpd.Client.ListPlaylists()
	if err != nil {
		config.Logger.Error().Err(err).Msg("could not get playlists!")
	}
	return FilesFromAttrs(playlistInfo)
}
