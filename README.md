# waitingroom

A tui mpd client

![screenshot](screenshots/waitingroom.png)


## installation

Grab a binary from [releases](https://codeberg.org/meatpuppet/waitingroom/releases), or install with go:

    go install codeberg.org/meatpuppet/waitingroom@latest


## initial configuration

Waitingroom respects `$MPD_HOST` and `$MPD_PORT` [environment variables](https://mpd.readthedocs.io/en/latest/client.html#environment-variables).  
If you have these set already for another client, no other configuration is necessary.

Otherwise, waitingroom is looking for a config file in `$HOME/.config/waitingroom/config.toml`.

To create the initial config file, run `waitingroom config create`, which will write a config file with the default configuration to that path.

A thing that you will then likely want to change is the `mpd_host`.


## default keybindings

The default keys are loosely based on vim motions.
For example, you can go to start/end of a list with `g`/`G`, or start a search with `/` - and then jump to the next (fuzzy) result with `n`.

Switch columns with `tab`, add songs (or whole folders) from the library with `a` after the currently playing song, or with `A` to the end of the Queue.

`space` toggles pause, `enter` plays from the start.

For a list of valid keys to use in your own config, see [the bubbletea keys]()!

### cheatsheet

#### movement

action | key
---|---
quit | `q`
move cursor up | `up`
move cursor down | `down`
switch active column | `tab`
---|---
go to beginning | `g`
go to end | `G`
---|---
open folder | `enter`
close folder | `backspace`
---|---
volume up | `+`
volume down | `-`

#### management

 action | key 
---|---
toggle play/pause | `space`
move item up | `<`
move item down | `>`
remove item | `delete`
fast forward 10s | `right`
rewind 10s | `left`
---|---
play after currently playing | `a`
append to end of queue | `A`
yank | `y`
paste | `p`
---|---
toggle library/playlist column | `shift+tab`
mark playlist / append to marked playlist| `m`
---|---
update library | `u`

#### searching

action | key
--- | ---
search | `/`
go to next match | `n`

#### queue keys

action | key
--- | ---
clear queue | `c`
play | `enter`


#### commands

`:save <playlistname>` save current queue to playlist

`:search <term>` search for `<term>`. opens a new list on the right (change with shift+tab)

`:sort {old(est), new(est), reset}` sort library by last modified date

`:q(uit)` exit waitingroom

## development

Remember to update the `CHANGELOG.md`!

### bumping

We use [bump2version](https://github.com/c4urself/bump2version) for version bumping.
You want to do something like

    bump2version patch
    # or, to test
    bump2version patch --dry-run --allow-dirty --verbose

then, push the new tag with `git push --follow-tags`

### releases

Make a new release with goreleaser !after! you bumped!

    # test
    goreleaser release --snapshot --clean

    # actual release
    GITEA_TOKEN=xxxx goreleaser release --clean


## why "waitingroom"?

<https://en.wikipedia.org/wiki/Waiting_Room_(song)>

Also, since this program is all about making a list of songs all waiting to be played...
