package waitingroom

import (
	"codeberg.org/meatpuppet/waitingroom/config"
	"codeberg.org/meatpuppet/waitingroom/list"
)

var libraryCache = map[string]CachedPath{}

type CachedPath struct {
	Cursor  int
	Content []list.Item
}

// store where the cursor is in which part of the library tree,
// so we return to the same place when going up a folder
func SetCacheCursorPath(path string, cursorPos int) {
	content := libraryCache[path]
	content.Cursor = cursorPos
	libraryCache[path] = content
}

func ResetCache() {
	libraryCache = map[string]CachedPath{}
}

func SetLibraryCacheItems(path string, items []list.Item) {
	cachedPath, ok := libraryCache[path]
	if !ok {
		config.Logger.Error().Msgf("cannot get cached path %s", path)
	} else {
		libraryCache[path] = CachedPath{
			Cursor:  cachedPath.Cursor,
			Content: items,
		}
	}
}

// return cursorpos and itemlist from cache
func (m *model) GetLibraryPathFromCache(path string) CachedPath {
	_, ok := libraryCache[path]
	if !ok {
		config.Logger.Debug().Msgf("cache miss for %s", path)
		cursor, files := m.MpdClient.GetLibraryPath(path)
		items := filesToItems(files)
		libraryCache[path] = CachedPath{
			Cursor:  cursor,
			Content: items,
		}
	} else {
		// c.Logger.Debug().Msgf("found %s in cache", path)
	}
	return libraryCache[path]
}
