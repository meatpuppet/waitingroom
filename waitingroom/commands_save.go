package waitingroom

import (
	"fmt"
	"strings"

	"codeberg.org/meatpuppet/waitingroom/mpdclient"
	tea "github.com/charmbracelet/bubbletea"
)

type SaveCommand struct{}

func playlistExists(existingPlaylists []mpdclient.File, playlistName string) bool {
	found := false
	for _, playlist := range existingPlaylists {
		if playlist.Path == playlistName {
			found = true
		}
	}
	return found
}

func (s SaveCommand) Run(m *model, terms []string) tea.Cmd {
	m.inputs.save = strings.Join(terms, " ")
	existingPlaylists := m.MpdClient.GetPlaylists()

	if playlistExists(existingPlaylists, m.inputs.save) {
		prompt := fmt.Sprintf("override existing playlist %s?", m.inputs.save)
		yesOptions := []string{"y", "Y"}
		runAfterConfirmFunc := func(m *model) {
			save := m.inputs.save
			m.MpdClient.PlaylistRemove(save)
			err := m.MpdClient.PlaylistSave(save)
			if err != nil {
				m.showMessage(err.Error())
			}
		}

		m.PushConfirmMessage(prompt, yesOptions, runAfterConfirmFunc)

	} else {
		err := m.MpdClient.PlaylistSave(m.inputs.save)
		m.inputs.save = ""
		if err != nil {
			m.showMessage(err.Error())
		}
	}
	return nil
}

func (s SaveCommand) CompleteCandidates(m *model, input string) []string {
	items := m.Playlists.GetItems()
	playlistNames := []string{}

	for _, i := range items {
		playlistNames = append(playlistNames, i.GetPath())
	}
	return playlistNames
}
