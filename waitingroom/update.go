package waitingroom

import (
	"time"

	"codeberg.org/meatpuppet/waitingroom/config"
	"codeberg.org/meatpuppet/waitingroom/mpdclient"
	"codeberg.org/meatpuppet/waitingroom/statusbar"
	tea "github.com/charmbracelet/bubbletea"
)

type needReconnectMsg bool

// triggers needReconnectMsg, which in return triggers the next ping in update
func pingCmd(m model) tea.Cmd {
	return tea.Tick(time.Second*10, func(t time.Time) tea.Msg {
		config.Logger.Debug().Msg("ping!")
		err := m.MpdClient.Ping()
		if err != nil {
			config.Logger.Warn().Msg("connection lost!!")
			return needReconnectMsg(true)
		}
		return needReconnectMsg(false)
	})
}

func (m *model) updateCurrentSong() {
	config.Logger.Debug().Msg("updating current song")
	m.currentTrack = m.MpdClient.CurrentSong()
	state := m.MpdClient.Status()
	config.Logger.Debug().Msgf("initial elapsed: %d", state.Elapsed)

	m.Statusbar.SetPlaying(m.currentTrack, state.Elapsed, state.State, state.Volume)
	m.Queue.SetActiveItem(m.currentTrack.Pos)
}

func (m *model) updateViewSizes() {
	config.Logger.Debug().Msgf("window size is %dx%d", m.width, m.height)
	for _, view := range m.GetViews() {
		// if too small, we use the whole width for a column, and only render the active column in view
		width := m.width
		if m.width > config.Config.MinColumnWidth {
			width = (m.width / len(m.GetViews()))
		}
		config.Logger.Debug().Msgf("setting column width to %d", width)
		height := m.height - 1
		view.SetSize(width, height)
	}
	m.Statusbar.SetSize(m.width, m.height)
}

func (m model) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	var cmd tea.Cmd
	cmds := make([]tea.Cmd, 0)

	switch msg := msg.(type) {

	case mpdclient.SubsystemEventMsg:
		config.Logger.Info().Msgf("subsystem message: %s", msg)
		switch msg {
		case "update":
			m.reloadLibrary()
			m.reloadPlaylists()
		case "player":
			m.updateCurrentSong()
		case "mixer":
			m.updateCurrentSong()

		case "playlist":
			m.ReloadQueue()
		case "stored_playlist":
			m.reloadPlaylists()
			m.UpdateOpenPlaylist()
		}

	case needReconnectMsg:
		if msg == true {
			config.Logger.Info().Msg("reconnecting")
			m.MpdClient = mpdclient.GetClient(config.Config.MpdHost, config.Config.MpdPort, config.Config.MpdPassword)
			m.ReloadQueue()
		}
		cmds = append(cmds, pingCmd(m))

	case statusbar.TickMsg:
		// config.Logger.Debug().Msg("*tick*")
		m.Statusbar, cmd = m.Statusbar.Update(msg)
		cmds = append(cmds, cmd)

	case tea.WindowSizeMsg:
		m.height = msg.Height
		m.width = msg.Width
		m.updateViewSizes()

	case tea.KeyMsg:

		switch m.mode {
		case CommandInputMode:
			cmd = m.HandleCommandInput(msg)
			cmds = append(cmds, cmd)
			return m, tea.Batch(cmds...)
		case SearchInputMode:
			m.HandleSearchInput(msg)
			m.ActiveView.Search(m.inputs.search)
			return m, tea.Batch(cmds...)
		case ConfirmMode:
			m.HandleConfirm(msg)
			return m, tea.Batch(cmds...)
		default:
			switch m.ActiveView {
			case m.Queue:
				m.HandleQueueKeys(msg)
			case m.Library:
				m.HandleLibraryKeys(msg)
			case m.Results:
				m.HandleResultsKeys(msg)
			case m.Playlists:
				m.HandlePlaylistsKeys(msg)
			case m.Playlist:
				m.HandlePlaylistKeys(msg)
			}
		}
		cmd = m.HandleGlobalKeys(msg)
		cmds = append(cmds, cmd)
	}

	// var cmd tea.Cmd
	// m.Playlist, cmd = m.Playlist.Update(msg)
	// cmds = append(cmds, cmd)

	// Return the updated model to the Bubble Tea runtime for processing.
	// Note that we're not returning a command.
	return m, tea.Batch(cmds...)
}
