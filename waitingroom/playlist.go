package waitingroom

import (
	"codeberg.org/meatpuppet/waitingroom/config"
	"codeberg.org/meatpuppet/waitingroom/list"
	"codeberg.org/meatpuppet/waitingroom/mpdclient"
)

func (m *model) getPlaylists() []list.Item {
	config.Logger.Info().Msgf("getting playlists")
	items := m.MpdClient.GetPlaylists()
	playlist := filesToItems(items)
	return playlist

}

func (m *model) getPlaylist(uri string) []list.Item {
	config.Logger.Debug().Msgf("getting %s", uri)
	items := m.MpdClient.GetPlaylist(uri)
	playlist := filesToItems(items)
	return playlist
}

func (m *model) UpdateOpenPlaylist() {
	if m.Playlist == nil {
		return
	}
	playlistItems := m.getPlaylist(m.Playlist.GetId())
	_, cursorPos := m.Playlist.GetCursorItem()
	newCursorPos := cursorPos
	if newCursorPos > len(playlistItems)-1 {
		newCursorPos = 0
	}
	m.Playlist.SetItems(playlistItems, cursorPos, m.Playlist.GetId())
}

func (m *model) reloadPlaylists() {
	_, oldCursorPos := m.Playlists.GetCursorItem()
	playlists := m.getPlaylists()

	newCursorPos := oldCursorPos
	if newCursorPos > len(playlists) {
		newCursorPos = len(playlists)
	}

	// find marked playlist, update struct
	for _, i := range playlists {
		f := i.(*mpdclient.File)
		if f.Path == m.MarkedPlaylistPath {
			f.IsMarked = true
			m.MarkedPlaylist = f
		}
	}

	m.Playlists.SetItems(playlists, newCursorPos, "playlists")
}

func (m *model) AppendToMarkedPlaylist(i list.Item) error {
	markedPlaylist := m.MarkedPlaylist.(*mpdclient.File)

	switch i.(type) {

	case *mpdclient.File:
		file := i.(*mpdclient.File)
		if file.FileType == mpdclient.PlaylistType {
		} else {
			// files or directories work the same
			config.Logger.Info().Msgf("adding %s", file.Path)
			err := m.MpdClient.PlaylistAdd(markedPlaylist.Path, file.Path)
			if err != nil {
				return err
			}
		}

	case *mpdclient.Track:
		file := i.(*mpdclient.Track)
		// files or directories work the same
		config.Logger.Info().Msgf("adding %s", file.Path)
		err := m.MpdClient.PlaylistAdd(markedPlaylist.Path, file.Path)
		if err != nil {
			return err
		}
	}
	return nil
}
