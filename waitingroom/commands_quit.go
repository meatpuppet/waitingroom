package waitingroom

import (
	tea "github.com/charmbracelet/bubbletea"
)

type QuitCommand struct{}

func (s QuitCommand) Run(m *model, terms []string) tea.Cmd {
	return tea.Quit
}

func (s QuitCommand) CompleteCandidates(m *model, input string) []string {
	return []string{}
}
