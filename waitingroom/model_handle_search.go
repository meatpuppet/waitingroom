package waitingroom

import (
	tea "github.com/charmbracelet/bubbletea"
)

/*
*  set searchterm in all lists
*  "/" search, not :search
 */
func (m *model) HandleSearchInput(msg tea.KeyMsg) tea.Cmd {
	var cmd tea.Cmd

	switch msg.String() {
	case "enter":
		m.mode = NormalMode
		m.ResetTextInput()

		if m.inputs.search == "" {
			m.Statusbar.SetOptionText("")
		} else {
			m.Statusbar.SetOptionText("search:" + m.inputs.search)
		}
	case "esc":
		m.mode = NormalMode
		m.ResetTextInput()
		m.inputs.search = ""

	default:
		m.TextInput, cmd = m.TextInput.Update(msg)
		// live update search term
		m.inputs.search = m.TextInput.Value()
	}
	return cmd
}
