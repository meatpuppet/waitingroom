package waitingroom

import (
	"errors"
	"fmt"
	"slices"
	"strings"

	tea "github.com/charmbracelet/bubbletea"
)

func (m *model) PushConfirmMessage(msg string, yesOptions []string, afterConfirmFunc func(m *model)) {
	message := confirmMessage{
		Prompt:              msg,
		YesOptions:          yesOptions,
		RunAfterConfirmFunc: afterConfirmFunc,
	}
	m.messages = append(m.messages, message)
	m.SetConfirmMode()
}

func (m *model) popConfirmMessage() (confirmMessage, error) {
	var message confirmMessage
	if len(m.messages) > 0 {
		message, m.messages = m.messages[0], m.messages[1:]
		return message, nil
	}
	return message, errors.New("queue empty")
}

func (m *model) SetConfirmMode() {
	m.ResetTextInput()
	m.mode = ConfirmMode

	msg := m.messages[0]
	if len(msg.YesOptions) > 0 {
		m.TextInput.Prompt = fmt.Sprintf("%s [%s]", msg.Prompt, strings.Join(msg.YesOptions, ""))
	} else {
		m.TextInput.Prompt = fmt.Sprintf("%s", msg.Prompt)
	}
}

/*
* handles key presses after SetConfirmMode
*/
func (m *model) HandleConfirm(msg tea.KeyMsg) tea.Cmd {
	message, _ := m.popConfirmMessage()
	if slices.Contains(message.YesOptions, msg.String()) {
		message.RunAfterConfirmFunc(m)
	}
	m.ResetAfterConfirm()

	if len(m.messages) > 0 {
		m.SetConfirmMode()
	}
	return nil
}

func (m *model) ResetAfterConfirm() {
	m.mode = NormalMode
}

/*
* just a wrapper with an empty runAfter function, just to show a message
*/
func (m *model) showMessage(s string) {
	prompt := fmt.Sprintf(s)
	yesOptions := []string{} // yesoptions are irrelevant
	runAfterConfirmFunc := func(m *model) {}
	m.PushConfirmMessage(prompt, yesOptions, runAfterConfirmFunc)
}
