package waitingroom

import (
	"codeberg.org/meatpuppet/waitingroom/mpdclient"
	tea "github.com/charmbracelet/bubbletea"
)

func (m *model) HandleLibraryKeys(msg tea.KeyMsg) tea.Cmd {
	m.HandleGenericListKeys(msg, m.Library)

	switch msg.String() {

	case m.Keys.General.Enter:
		i, oldCursorPos := m.Library.GetCursorItem()
		cursorItem := i.(*mpdclient.File)
		if cursorItem.FileType == mpdclient.PlaylistType {
			SetCacheCursorPath(m.Library.GetId(), oldCursorPos)
			playlistItems := m.getPlaylist(cursorItem.Path)
			m.Library.SetItems(playlistItems, 0, cursorItem.Path)
		} else {
			SetCacheCursorPath(m.Library.GetId(), oldCursorPos)
			m.openPath(*cursorItem)
		}

	case m.Keys.General.FolderUp:
		m.openParent()

	case m.Keys.General.PlayNext:
		// get playlist length before adding
		i, _ := m.Library.GetCursorItem()
		m.PlayNext(i)
		m.Library.SetCursorDown()

	case m.Keys.General.Append:
		// add to the end
		i, _ := m.Library.GetCursorItem()
		m.Append(i)
		m.Library.SetCursorDown()

	case m.Keys.General.GoToNextMatch:
		m.Library.SetCursorToNextMatch(m.inputs.search)
	case m.Keys.General.MarkPlaylist:
		i, _ := m.Library.GetCursorItem()
		if m.MarkedPlaylist != nil {
			m.AppendToMarkedPlaylist(i)
			m.Library.SetCursorDown()
		}
	}
	return nil
}
