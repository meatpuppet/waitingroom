package waitingroom

import (
	tea "github.com/charmbracelet/bubbletea"
)

func (m *model) HandlePlaylistKeys(msg tea.KeyMsg) tea.Cmd {
	m.HandleGenericListKeys(msg, m.Playlist)
	switch msg.String() {
	case m.Keys.General.RemoveItem:
		_, pos := m.Playlist.GetCursorItem()
		m.MpdClient.PlaylistDelete(m.Playlist.GetId(), pos)
		if pos < m.Playlist.Len() {
			m.Playlist.SetCursorUp()
		}

	case m.Keys.General.MoveItemUp:
		_, pos := m.Playlist.GetCursorItem()
		newPos := pos + 1
		if newPos < m.Playlist.Len() {
			m.MpdClient.PlaylistMove(m.Playlist.GetId(), pos, newPos)
			m.Playlist.SetCursorDown()
		}

	case m.Keys.General.MoveItemDown:
		_, pos := m.Playlist.GetCursorItem()
		newPos := pos - 1
		if newPos >= 0 {
			m.MpdClient.PlaylistMove(m.Playlist.GetId(), pos, newPos)
			m.Playlist.SetCursorUp()
		}

	case m.Keys.General.PlayNext:
		i, _ := m.Playlist.GetCursorItem()
		m.PlayNext(i)
		m.Playlist.SetCursorDown()

	case m.Keys.General.Append:
		i, _ := m.Playlist.GetCursorItem()
		m.Append(i)
		m.Playlist.SetCursorDown()

	case m.Keys.General.GoToNextMatch:
		m.Playlist.SetCursorToNextMatch(m.inputs.search)

	case m.Keys.General.FolderUp:
		m.Playlist = nil
		m.SetRightView(m.Playlists)
	}
	return nil
}

