package waitingroom

import (
	"codeberg.org/meatpuppet/waitingroom/config"
	"codeberg.org/meatpuppet/waitingroom/list"
	"codeberg.org/meatpuppet/waitingroom/mpdclient"
	"codeberg.org/meatpuppet/waitingroom/statusbar"
	"github.com/charmbracelet/bubbles/textinput"
	tea "github.com/charmbracelet/bubbletea"
)

type Mode int

const (
	NormalMode Mode = iota
	SearchInputMode
	CommandInputMode
	ConfirmMode
)

type inputs struct {
	activeViewBefore *list.Model
	search           string
	save             string
	command          string
}

/*
* messages to show to the user
*/
type confirmMessage struct {
	// the prompt shown to the user
	Prompt     string
	// what to press to say "yes" to the prompt
	YesOptions []string
	// what you want to do after confirm
	// runs if prompt is accepted with one of the YesOptions
	RunAfterConfirmFunc func(*model)
}

type model struct {
	width  int
	height int

	mode   Mode
	inputs inputs

	messages []confirmMessage

	Library   *list.Model
	Queue     *list.Model
	Playlist  *list.Model
	Playlists *list.Model
	Results   *list.Model

	// dirty hack: on list update event, all items will be recreated
	// so the "marked" playlist isnt the same object anymore.
	// we store the playlist path, to update marked playlist on reload.
	MarkedPlaylistPath string
	MarkedPlaylist     list.Item

	clipboard list.Item

	visibleViews []*list.Model

	// library, playlists
	rightSideViews []*list.Model

	// one of the open views that has the cursor
	ActiveView *list.Model

	Statusbar statusbar.Model
	TextInput textinput.Model

	currentTrack mpdclient.Track

	MpdClient *mpdclient.MpdClient
	Keys      config.Keys
}

func (m model) GetViews() []*list.Model {
	return m.visibleViews
}

func (m model) Init() tea.Cmd {
	return tea.Batch(pingCmd(m), statusbar.TickCmd())
}

func filesToItems(files []mpdclient.File) []list.Item {
	items := make([]list.Item, len(files))
	for idx, i := range files {
		i := i
		items[idx] = &i
	}
	return items
}

func tracksToItems(tracks []mpdclient.Track) []list.Item {
	items := []list.Item{}
	for _, i := range tracks {
		i := i
		items = append(items, &i)
	}
	return items
}

func InitialModel(c config.Configuration) model {
	mpdClient := mpdclient.GetClient(c.MpdHost, c.MpdPort, c.MpdPassword)

	queue := list.New([]list.Item{}, 0, 0, "Queue", false, "")
	library := list.New([]list.Item{}, 0, 0, "Library", false, "/")
	playlists := list.New([]list.Item{}, 0, 0, "Playlists", false, "playlists")
	searchResults := list.New([]list.Item{}, 0, 0, "Results", false, "results")

	queue.SetIsActive(true)

	statusBar := statusbar.New()
	textInput := textinput.New()
	textInput.Focus()
	textInput.ShowSuggestions = true

	visibleViews := []*list.Model{&queue, &library}
	rightSideViews := []*list.Model{&library, &playlists}

	m := model{
		Library: &library,
		Queue:   &queue,

		ActiveView: &queue,

		Playlist:  nil,
		Playlists: &playlists,
		Results:   &searchResults,

		Statusbar: statusBar,
		TextInput: textInput,
		MpdClient: mpdClient,

		width:  0,
		height: 0,

		Keys: c.Keys,

		visibleViews:   visibleViews,
		rightSideViews: rightSideViews,
	}

	m.ReloadQueue()
	m.reloadPlaylists()
	m.reloadLibrary()
	return m
}

func (m *model) setActiveView(view *list.Model) {
	m.ActiveView.SetIsActive(false)
	m.ActiveView = view
	m.ActiveView.SetIsActive(true)
}

func (m *model) getActiveViewIndex() int {
	for idx, view := range m.visibleViews {
		if view == m.ActiveView {
			return idx
		}
	}
	config.Logger.Warn().Msg("could not get active view index")
	return 0
}

func (m *model) getRightViewIndex() int {
	for idx, view := range m.rightSideViews {
		if view == m.visibleViews[1] {
			return idx
		}
	}
	config.Logger.Warn().Msg("could not get right side view index")
	return 0
}

func (m *model) iterateActiveView() {
	activeViewIndex := m.getActiveViewIndex()
	activeViewIndex += 1
	activeViewIndex %= len(m.GetViews())
	views := m.GetViews()
	m.setActiveView(views[activeViewIndex])
}

func (m *model) iterateRightView() {
	rightSideViewIndex := m.getRightViewIndex()
	rightSideViewIndex += 1
	rightSideViewIndex %= len(m.rightSideViews)

	m.visibleViews[1] = m.rightSideViews[rightSideViewIndex]
	// always switch to right side view when iterating
	m.setActiveView(m.visibleViews[1])
	m.updateViewSizes()
}

func (m *model) SetCommandMode() {
	m.TextInput.Prompt = ":"
	m.mode = CommandInputMode
}

func (m *model) SetSearchMode() {
	m.TextInput.Prompt = "/"
	m.mode = SearchInputMode
}

func (m *model) SetRightView(view *list.Model) {
	activeViewIndex := m.getActiveViewIndex()
	m.visibleViews[1] = view
	if activeViewIndex == 1 {
		m.setActiveView(m.visibleViews[1])
	}
	m.updateViewSizes()
}

func (m *model) ResetTextInput() {
	m.TextInput.Reset()
	m.TextInput.Prompt = ""
	m.TextInput.SetSuggestions([]string{})
}
