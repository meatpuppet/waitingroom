package waitingroom

import (
	"codeberg.org/meatpuppet/waitingroom/config"
	"codeberg.org/meatpuppet/waitingroom/list"
	"codeberg.org/meatpuppet/waitingroom/mpdclient"
	tea "github.com/charmbracelet/bubbletea"
)

func (m *model) HandleGlobalKeys(msg tea.KeyMsg) tea.Cmd {
	switch msg.String() {
	// These keys should exit the program.
	case m.Keys.General.Quit:
		return tea.Quit

		// switch active column
	case m.Keys.General.SwitchColumn:
		m.ActiveView.SetIsActive(false)
        m.iterateActiveView()
		m.ActiveView.SetIsActive(true)
		return nil

	case m.Keys.General.TogglePlaylistColumn:
		m.iterateRightView()

	case m.Keys.General.Command:
		m.SetCommandMode()
	case m.Keys.General.Search:
		m.SetSearchMode()

	case m.Keys.General.PlayToggle:
		if m.Statusbar.State == mpdclient.PlayingState {
			m.MpdClient.Pause(true)
		} else {
			m.MpdClient.Pause(false)
		}
	case m.Keys.General.UpdateLibrary:
		m.MpdClient.UpdateLibrary()

	case m.Keys.General.FastForward:
		status := m.MpdClient.Status()
		m.MpdClient.Seek(status.Song, status.Elapsed + config.Config.SkipSeconds)
		
	case m.Keys.General.Rewind:
		status := m.MpdClient.Status()
		m.MpdClient.Seek(status.Song, status.Elapsed - config.Config.SkipSeconds)

	case m.Keys.General.VolumeUp:
		status := m.MpdClient.Status()
		m.MpdClient.SetVolume(status.Volume + 5)

	case m.Keys.General.VolumeDown:
		status := m.MpdClient.Status()
		m.MpdClient.SetVolume(status.Volume - 5)

	default:
    }
	return nil
}

func (m *model) HandleGenericListKeys(msg tea.KeyMsg, list *list.Model) tea.Cmd {
	switch msg.String() {
	case m.Keys.General.Up:
		list.SetCursorUp()
	case m.Keys.General.Down:
		list.SetCursorDown()
	case m.Keys.General.GoToBeginning:
		list.SetCursor(0)
	case m.Keys.General.GoToEnd:
		list.SetCursor(list.Len() - 1)
	}
	return nil
}

