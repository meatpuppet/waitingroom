package waitingroom

import (
	"codeberg.org/meatpuppet/waitingroom/config"
	"codeberg.org/meatpuppet/waitingroom/list"
	"codeberg.org/meatpuppet/waitingroom/mpdclient"
	tea "github.com/charmbracelet/bubbletea"
)

func (m *model) HandlePlaylistsKeys(msg tea.KeyMsg) tea.Cmd {
	m.HandleGenericListKeys(msg, m.Playlists)
	switch msg.String() {
	// todo: ask before delete
	//
	// case m.Keys.General.RemoveItem:
	// 	_, pos := list.GetCursorItem()
	// 	lenAfterDelete := list.Len() - 1
	// 	m.MpdClient.PlaylistDelete(list.GetId(), pos)
	// 	if pos > lenAfterDelete {
	// 		list.SetCursorUp()
	// 	}
	case m.Keys.General.PlayNext:
		i, _ := m.Playlists.GetCursorItem()
		m.PlayNext(i)

	case m.Keys.General.Append:
		i, _ := m.Playlists.GetCursorItem()
		m.Append(i)

	case m.Keys.General.GoToNextMatch:
		m.Playlists.SetCursorToNextMatch(m.inputs.search)

	case m.Keys.General.Enter:
		i, oldCursorPos := m.Playlists.GetCursorItem()
		cursorItem := i.(*mpdclient.File)
		config.Logger.Info().Msgf("opening playlist %+v", cursorItem)
		SetCacheCursorPath(m.Playlists.GetId(), oldCursorPos)
		playlistItems := m.getPlaylist(cursorItem.Path)

		// todo set list title
		playlist := list.New(playlistItems, 0, 0, cursorItem.Path, false, cursorItem.Path)
		m.Playlist = &playlist

		m.SetRightView(m.Playlist)
		m.setActiveView(m.Playlist)
		m.updateViewSizes()

	case m.Keys.General.MarkPlaylist:
		i, _ := m.Playlists.GetCursorItem()

		// item already marked -> toggle
		if m.MarkedPlaylist == i {
			markedItem := m.MarkedPlaylist.(*mpdclient.File)
			markedItem.IsMarked = false
			m.MarkedPlaylist = nil
			m.MarkedPlaylistPath = ""
		} else {
			// if there is a marked list already
			// remove old mark
			if m.MarkedPlaylist != nil {
				markedItem := m.MarkedPlaylist.(*mpdclient.File)
				markedItem.IsMarked = false
			}
			// set new mark
			m.MarkedPlaylist = i
			markedItem := m.MarkedPlaylist.(*mpdclient.File)
			m.MarkedPlaylistPath = markedItem.Path
			cursorItem := i.(*mpdclient.File)
			if cursorItem.FileType == mpdclient.PlaylistType {
				cursorItem.IsMarked = true
			}
		}
	}
	return nil
}
