package waitingroom

import (
	"fmt"

	"codeberg.org/meatpuppet/waitingroom/config"
	tea "github.com/charmbracelet/bubbletea"
)

func (m *model) HandleQueueKeys(msg tea.KeyMsg) tea.Cmd {
	m.HandleGenericListKeys(msg, m.Queue)

	switch msg.String() {

	case m.Keys.Queue.Clear:
		prompt := fmt.Sprintf("clear queue?")
		yesOptions := []string{"y", "Y"}
		runAfterConfirmFunc := func(m *model) {
			config.Logger.Debug().Msg("clearing queue")
			m.MpdClient.Clear()
		}
		m.PushConfirmMessage(prompt, yesOptions, runAfterConfirmFunc)

	case m.Keys.Queue.Play:
		config.Logger.Info().Msgf("playing!")
		_, pos := m.Queue.GetCursorItem()
		m.MpdClient.Play(pos)

	case m.Keys.General.RemoveItem:
		item, pos := m.Queue.GetCursorItem()
		m.clipboard = item

		if pos >= m.Queue.Len()-1 {
			m.Queue.SetCursorUp()
		}
		m.MpdClient.Delete(pos, -1)

	case m.Keys.General.Yank:
		item, _ := m.Queue.GetCursorItem()
		m.clipboard = item

	case m.Keys.General.Paste:
		if m.clipboard != nil {
			m.addTrackAfterCursor(m.clipboard)
			m.Queue.SetCursorDown()
		}

	case m.Keys.General.MoveItemUp:
		_, pos := m.Queue.GetCursorItem()
		newPos := pos + 1
		if newPos < m.Queue.Len() {
			m.MpdClient.Move(pos, -1, newPos)
			m.Queue.SetCursorDown()
			if pos == m.Queue.GetActiveItem() {
				m.Queue.SetActiveItem(pos)
			}
		}

	case m.Keys.General.MoveItemDown:
		_, pos := m.Queue.GetCursorItem()
		newPos := pos - 1
		if newPos >= 0 {
			m.MpdClient.Move(pos, -1, newPos)
			m.Queue.SetCursorUp()
			if pos == m.Queue.GetActiveItem() {
				m.Queue.SetActiveItem(pos)
			}
		}

	case m.Keys.General.GoToNextMatch:
		m.Queue.SetCursorToNextMatch(m.inputs.search)
	
	case m.Keys.General.MarkPlaylist:
		i, _ := m.Queue.GetCursorItem()
		if m.MarkedPlaylist != nil {
			err := m.AppendToMarkedPlaylist(i)
			if err != nil {
				m.showMessage(err.Error())
			}
			m.Queue.SetCursorDown()
		} else {
			m.showMessage("no playlist marked")
		}

	case m.Keys.General.Search:
		m.SetSearchMode()

	}
	return nil
}
