package waitingroom

import (
	"sort"

	"codeberg.org/meatpuppet/waitingroom/list"
	tea "github.com/charmbracelet/bubbletea"
)

type ByLastModified []list.Item

func (a ByLastModified) Len() int {
	return len(a)
}
func (a ByLastModified) Less(i, j int) bool {
	return a[i].GetLastModified().Before(a[j].GetLastModified())
}
func (a ByLastModified) Swap(i, j int) {
	a[i], a[j] = a[j], a[i]
}

type SortCommand struct{}

func SortItemsByLastModified(items []list.Item, asc bool) []list.Item {
	if asc {
		sort.Sort(sort.Reverse(ByLastModified(items)))
	} else {
		sort.Sort(ByLastModified(items))
	}
	return items
}

func (s SortCommand) Run(m *model, terms []string) tea.Cmd {
	currentView := m.visibleViews[1]
	items := currentView.GetItems()
	cursorItem, _ := currentView.GetCursorItem()
	if len(terms) > 0 {
		switch terms[0] {
		case "oldest", "old":
			items = SortItemsByLastModified(items, false)
		case "newest", "new":
			items = SortItemsByLastModified(items, true)
		case "reset":
			if currentView == m.Library {
				m.reloadLibrary()
				SetLibraryCacheItems(m.Library.GetId(), items)
			}
			if currentView == m.Playlists {
				m.reloadPlaylists()
			}
			// a playlist doenst have a sorting function in mpd
			// if currentView == m.Playlist {}
			items = currentView.GetItems()
		default:
			items = SortItemsByLastModified(items, false)
		}
	}
	currentView.SetItems(items, 0, cursorItem.GetPath())
	return nil
}

func (s SortCommand) CompleteCandidates(m *model, input string) []string {
	return []string{"new", "newest", "old", "oldest"}
}
