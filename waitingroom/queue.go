package waitingroom

import (
	"fmt"

	"codeberg.org/meatpuppet/waitingroom/config"
	"codeberg.org/meatpuppet/waitingroom/list"
	"codeberg.org/meatpuppet/waitingroom/mpdclient"
)

// append a library item to the queue
func (m model) Append(i list.Item) {
	switch i.(type) {
	case *mpdclient.File:
		file := i.(*mpdclient.File)
		if file.FileType == mpdclient.PlaylistType {
			err := m.MpdClient.PlaylistLoad(file.GetPath(), -1, -1)
			if err != nil {
				config.Logger.Error().Err(err)
				m.showMessage(fmt.Sprintf("error: %v", err))
			}
		} else {
			// files or directories work the same
			config.Logger.Info().Msgf("adding %s", file.Path)
			err := m.MpdClient.Add(file.GetPath())
			if err != nil {
				config.Logger.Error().Err(err)
				m.showMessage(fmt.Sprintf("error: %v", err))
			}
		}
	case *mpdclient.Track:
		config.Logger.Info().Msgf("adding %s", i.GetPath())
		err := m.MpdClient.Add(i.GetPath())
		if err != nil {
			config.Logger.Error().Err(err)
			m.showMessage(fmt.Sprintf("error: %v", err))
		}
	default:
		config.Logger.Error().Msgf("unknown type of %+v", i)
	}
}

func (m model) addTrackAfterCursor(i list.Item) {
	_, pos := m.Queue.GetCursorItem()

	switch i.(type) {
	case *mpdclient.File:
		file := i.(*mpdclient.File)
		if file.FileType == mpdclient.PlaylistType {
			config.Logger.Info().Msgf("append after cursor not implemented for playlists")
		} else {
			// files or directories work the same
			config.Logger.Info().Msgf("adding %s", file.Path)
			_, err := m.MpdClient.AddID(file.Path, pos+1)
			if err != nil {
				m.showMessage(fmt.Sprintf("error: %v", err))
				config.Logger.Error().Err(err)
			}
		}

	case *mpdclient.Track:
		config.Logger.Info().Msgf("adding %s", i.GetPath())
		_, err := m.MpdClient.AddID(i.GetPath(), pos+1)
		if err != nil {
			config.Logger.Error().Err(err)
		}
	default:
		config.Logger.Error().Msgf("unknown type of %+v", i)
	}
}

func (m *model) PlayNext(item list.Item) {
	// get playlist length before adding
	statusBefore := m.MpdClient.Status()

	m.Append(item)
	statusAfter := m.MpdClient.Status()

	from := statusBefore.PlaylistLength
	to := statusAfter.PlaylistLength
	newPos := statusAfter.Song + 1

	err := m.MpdClient.Move(from, to, newPos)
	if err != nil {
		m.showMessage(fmt.Sprintf("error: %v", err))
		config.Logger.Error().Err(err)
	}
}


// (re-)initialize the queue and the current playing song
func (m *model) ReloadQueue() {
	playlist := m.MpdClient.GetQueue()
	_, cursorPos := m.Queue.GetCursorItem()
	newCursorPos := cursorPos
	if newCursorPos > len(playlist)-1 {
		newCursorPos = 0
	}
	m.Queue.SetItems(tracksToItems(playlist), newCursorPos, "")
	m.updateCurrentSong()
}
