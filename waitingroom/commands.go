package waitingroom

import tea "github.com/charmbracelet/bubbletea"

type Command interface {
	Run(m *model, input []string) tea.Cmd
	CompleteCandidates(m *model, input string) []string
}


var commands = map[string]Command{
	"search": SearchCommand{},
	"save": SaveCommand{},
	"sort": SortCommand{},
	"q": QuitCommand{},
	"quit": QuitCommand{},
}
