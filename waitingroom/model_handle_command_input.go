package waitingroom

import (
	"fmt"
	"maps"
	"slices"
	"strings"

	tea "github.com/charmbracelet/bubbletea"
)

func (m *model) RunCommand() tea.Cmd {
	// split to command and args
	sp := strings.SplitN(m.inputs.command, " ", 2)
	if len(sp) == 0 {
		return nil
	}
	command := sp[0]
	commandNames := slices.Collect(maps.Keys(commands))

	if slices.Contains(commandNames, command) {
		cmd := commands[command].Run(m, sp[1:])
		return cmd
	} else {
		m.showMessage(fmt.Sprintf("command not found: %s", command))
		return nil
	}
}

func (m *model) getSuggestions(input string) []string {
	sp := strings.Split(input, " ")
	if len(sp) == 0 {
		return []string{}
	}
	command := sp[0]
	commandNames := slices.Collect(maps.Keys(commands))

	if slices.Contains(commandNames, command) {
		candidates := commands[command].CompleteCandidates(m, strings.Join(sp[1:], " "))
		completeCandidates := []string{}
		for _, i := range candidates {
			// textbox expects the whole thing, so join with command
			completeCandidates = append(completeCandidates, sp[0]+" "+i)
		}
		return completeCandidates
	} else {
		// not a complete command name, just return the valid commands
		return commandNames
	}
}

func (m *model) HandleCommandInput(msg tea.KeyMsg) tea.Cmd {
	var cmd tea.Cmd
	switch msg.String() {
	case "enter":
		m.mode = NormalMode
		m.inputs.command = m.TextInput.Value()
		m.RunCommand()
		m.ResetTextInput()
		teaCmd := m.RunCommand()
		return teaCmd
	case "esc":
		m.mode = NormalMode
		m.inputs.command = ""
		m.ResetTextInput()
	default:
		suggestions := m.getSuggestions(m.TextInput.Value())
		m.TextInput.SetSuggestions(suggestions)
		m.TextInput, cmd = m.TextInput.Update(msg)
	}

	return cmd
}
