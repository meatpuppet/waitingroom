package waitingroom

import (
	"fmt"
	"slices"
	"strings"

	tea "github.com/charmbracelet/bubbletea"
)

type SearchCommand struct{}

func (s SearchCommand) Run(m *model, terms []string) tea.Cmd {
	res, err := m.MpdClient.Search(strings.Join(terms, " "))
	if err != nil {
		m.showMessage(fmt.Sprintf("error find: %v", err))
	}
	m.Results.SetItems(tracksToItems(res), 0, "results")
	m.Results.SetTitle("Results: " + strings.Join(terms, " "))
	if !slices.Contains(m.rightSideViews, m.Results) {
		m.rightSideViews = append(m.rightSideViews, m.Results)
	}
	m.setActiveView(m.Results)
	m.SetRightView(m.Results)
	return nil
}

func (s SearchCommand) CompleteCandidates(m *model, input string) []string {
	return []string{}
}
