package waitingroom

import (
	"path/filepath"
	"strings"

	"codeberg.org/meatpuppet/waitingroom/config"
	"codeberg.org/meatpuppet/waitingroom/mpdclient"
)

/*
* return file folder, or path if its already a folder
*/
func getFileFolder(path string) string {
	
	if path[len(path)-1] != '/' {
		path = filepath.Dir(path)
		config.Logger.Debug().Msgf("path set to %s", path)
	}

	// Dir() returns "." for empty paths
	if path == "." {
		path = "/"
	}
	// if path is empty, use root
	if path == "" {
		path = "/"
	}
	return path
}

func (m *model) reloadLibrary() {
	// clear cache
	ResetCache()
	oldCursor, oldCursorPos := m.Library.GetCursorItem()

	// try to stay on the current path when updating the library
	// default to root
	path := "/"
	if oldCursor != nil {
		path = oldCursor.GetPath()
	}
	path = getFileFolder(path)	

	// fetch contents of the path we were on
	library := m.GetLibraryPathFromCache(path)

	newCursorPos := oldCursorPos
	if newCursorPos > len(library.Content) {
		newCursorPos = len(library.Content)
	}
	m.Library.SetItems(library.Content, newCursorPos, path)
}

func getParentPath(path string) string {
	if path == "/" {
		return "/"
	}
	split := strings.Split(path, "/")
	if len(split) > 1 {
		return strings.Join(split[:len(split)-1], "/")
	}
	return "/"
}

func (m *model) openPath(file mpdclient.File) {
	library := m.GetLibraryPathFromCache(file.Path)
	m.Library.SetItems(library.Content, library.Cursor, file.Path)
}

func (m *model) openParent() {
	path := m.Library.GetId()
	parentPath := getParentPath(path)
	config.Logger.Debug().Msgf("in %s, opening %s", path, parentPath)

	// set cursor to cached path before we leave
	_, oldCursorPos := m.Library.GetCursorItem()
	SetCacheCursorPath(path, oldCursorPos)

	cached := m.GetLibraryPathFromCache(parentPath)
	items := cached.Content
	m.Library.SetItems(items, cached.Cursor, parentPath)
}
