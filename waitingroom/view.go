package waitingroom

import (
	"codeberg.org/meatpuppet/waitingroom/config"
	"github.com/charmbracelet/lipgloss"
)

func (m model) View() string {
	
	// draw statusbar or textinput depending on mode
	statusBarView := m.Statusbar.View
	if m.mode != NormalMode {
		statusBarView = m.TextInput.View
	}

	viewResults := []string{}
	// two column layout
	if m.width > config.Config.MinColumnWidth {
		for _, view := range m.GetViews() {
			viewResults = append(viewResults, view.View())
		}
	} else {
		// one column layout
		viewResults = []string{m.ActiveView.View()}
	}

	return lipgloss.JoinVertical(
		lipgloss.Left,
		lipgloss.JoinHorizontal(
			lipgloss.Left,
			viewResults...,
		),
		statusBarView(),
	)
}
