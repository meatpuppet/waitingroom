package waitingroom

import (
	"codeberg.org/meatpuppet/waitingroom/mpdclient"
	tea "github.com/charmbracelet/bubbletea"
)

func (m *model) HandleResultsKeys(msg tea.KeyMsg) tea.Cmd {
	m.HandleGenericListKeys(msg, m.Results)

	switch msg.String() {

	case m.Keys.General.Enter:
		i, oldCursorPos := m.Results.GetCursorItem()
		cursorItem := i.(*mpdclient.File)
		if cursorItem.FileType == mpdclient.PlaylistType {
			SetCacheCursorPath(m.Results.GetId(), oldCursorPos)
			playlistItems := m.getPlaylist(cursorItem.Path)
			m.Results.SetItems(playlistItems, 0, cursorItem.Path)
		} else {
			SetCacheCursorPath(m.Results.GetId(), oldCursorPos)
			m.openPath(*cursorItem)
		}

	case m.Keys.General.FolderUp:
		m.openParent()

	case m.Keys.General.PlayNext:
		// get playlist length before adding
		i, _ := m.Results.GetCursorItem()
		m.PlayNext(i)
		m.Results.SetCursorDown()

	case m.Keys.General.Append:
		// add to the end
		i, _ := m.Results.GetCursorItem()
		m.Append(i)
		m.Results.SetCursorDown()

	case m.Keys.General.GoToNextMatch:
		m.Results.SetCursorToNextMatch(m.inputs.search)
	case m.Keys.General.MarkPlaylist:
		i, _ := m.Results.GetCursorItem()
		if m.MarkedPlaylist != nil {
			m.AppendToMarkedPlaylist(i)
			m.Results.SetCursorDown()
		}
	}
	return nil
}
