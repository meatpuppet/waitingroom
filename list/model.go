package list

import (
	"strings"

	"codeberg.org/meatpuppet/waitingroom/config"
	tea "github.com/charmbracelet/bubbletea"
	"github.com/charmbracelet/lipgloss"
	"github.com/lithammer/fuzzysearch/fuzzy"
)

type Model struct {
	items []Item
	title string

	// used as a reference for caching (its the library path)
	id string

	viewPortStart int // top line of the viewport

	width  int
	height int

	cursor     int
	cursorLine int // active line in rendered block (should always be in viewport)

	activeItem int

	isActive bool

	baseStyle lipgloss.Style
}

func (m *Model) SetTitle(title string) {
	m.title = title
}

func (m Model) Len() int {
	return len(m.items)
}

func (m Model) GetId() string {
	return m.id
}

func (m Model) GetDuration() int {
	duration := 0
	for _, i := range m.items {
		duration += i.Length()
	}
	return duration
}

func (m *Model) SetSize(width, height int) {
	m.width = width - 2
	m.height = height
	m.baseStyle = m.baseStyle.Height(height - 2).Width(width - 2)
	m.UpdateViewPort()
}

func (m Model) GetHeight() int {
	return m.height
}

func (m Model) GetTextHeight() int {
	return m.height - 2
}
func (m Model) GetTextWidth() int {
	return m.baseStyle.GetWidth()
}
func (m *Model) SetIsActive(isActive bool) {
	m.isActive = isActive
}

func (m *Model) SetActiveItem(pos int) {
	m.activeItem = pos
}
func (m *Model) GetActiveItem() int {
	return m.activeItem
}

func (m *Model) GetTitle() string {
	return " " + m.title + " "
}

func (m *Model) SetItems(items []Item, cursorPos int, id string) {
	m.id = id
	m.items = items
	m.cursor = cursorPos
	m.UpdateViewPort()
}

func (m *Model) SetCursor(c int) {
	m.cursor = c
	m.UpdateViewPort()
}

func (m *Model) SetCursorUp() {
	if m.cursor > 0 {
		m.cursor -= 1
	}
	m.UpdateViewPort()
}
func (m *Model) SetCursorDown() {
	if m.cursor < len(m.items)-1 {
		m.cursor += 1
	}
	m.UpdateViewPort()
}

func (m Model) GetCursorItem() (Item, int) {
	if len(m.items) == 0 {
		return nil, 0
	}

	if m.cursor < len(m.items) {
		return m.items[m.cursor], m.cursor
	}
	return m.items[len(m.items)-1], m.cursor
}

func (m Model) GetLastItem() (Item, int) {
	if len(m.items) == 0 {
		return nil, 0
	}
	pos := len(m.items) - 1
	return m.items[pos], pos
}

func (m *Model) AddItems(items []Item) {
	m.items = append(m.items, items...)
}

func (m Model) Init() tea.Cmd {
	return nil
}

func findMatch(items []Item, searchTerm string, startPos int) (index int, found bool) {
	if searchTerm == "" {
		return -1, false
	}
	// search "from where we are"
	for idx, i := range items[startPos:] {
		if fuzzy.MatchFold(searchTerm, i.Text()) || fuzzy.MatchFold(searchTerm, i.Group()) {
			index = startPos + idx
			return index, true
		}
	}
	// if not found, start at beginning
	for idx, i := range items[:startPos] {
		if fuzzy.MatchFold(searchTerm, i.Text()) || fuzzy.MatchFold(searchTerm, i.Group()) {
			return idx, true
		}
	}
	return -1, false
}

func (m *Model) SetCursorToNextMatch(searchTerm string) {
	startFrom := m.cursor + 1
	idx, found := findMatch(m.items, searchTerm, startFrom)
	if found {
		m.cursor = idx
		m.UpdateViewPort()
	}
	return
}

func (m *Model) SearchExact(searchTerm string) bool {
	for idx, item := range m.items {
		found := strings.HasPrefix(item.Text(), searchTerm)
		if found {
			m.cursor = idx
			m.UpdateViewPort()
			return true
		}
	}
	return false
}

func (m *Model) Search(searchTerm string) {
	idx, found := findMatch(m.items, searchTerm, m.cursor)
	if found {
		m.cursor = idx
		m.UpdateViewPort()
	}
}

func (m Model) GetItems() []Item {
	return m.items
}

func New(items []Item, width, height int, title string, isActive bool, id string) Model {
	m := Model{
		width:      width,
		height:     height,
		cursor:     0,
		cursorLine: 0,
		items:      items,
		title:      title,
		isActive:   isActive,
		baseStyle:  config.Styles.BorderBase,

		activeItem: -1,

		id: id,
	}

	return m
}
