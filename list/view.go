package list

import (
	"fmt"
	"strings"

	"codeberg.org/meatpuppet/waitingroom/config"
	"github.com/charmbracelet/lipgloss"
)

const borderwidth = 2

func HumanizeTime(s int) string {
	minutes := s / 60
	seconds := s % 60
	str := fmt.Sprintf("%d:%02d", minutes, seconds)
	return str
}


func (m Model) RenderLine(text, textRight string, style lipgloss.Style) string {
	width := m.GetTextWidth() - 4

	// todo: isnt there a lipgloss thing to do this?

	// trim "left text" to the right width, add whitespace to the right
	leftWidth := width - len([]rune(textRight))

	// add whitespace to the right
	// trim, if the string was longer
	textLeft := fmt.Sprintf("%-*s", leftWidth, text)
	textLeft = string([]rune(textLeft)[:leftWidth])

	text = textLeft + textRight

	return style.MaxWidth(width).Render(text)
}

func (m Model) renderGroup(group string, style lipgloss.Style) string {
	return m.RenderLine(group, "", style)
}

func (m Model) renderItem(item Item, baseStyle lipgloss.Style, hasCursor, isActive bool) string {
	prefix := "  "
	if hasCursor {
		if m.isActive {
			baseStyle = config.Styles.CursorItemStyle.Inherit(baseStyle)
		} else {
			baseStyle = config.Styles.InactiveCursorItemStyle.Inherit(baseStyle)
		}
	}
	if isActive {
		baseStyle = config.Styles.ActiveItemStyle.Inherit(baseStyle)
		prefix = "> "
	}
	return m.RenderLine(prefix+item.Text(), item.TextRight(), baseStyle)
}

func (m Model) drawViewPort(lines []string) string {
	if m.getViewPortEnd() < len(lines) {
		return strings.Join(lines[m.viewPortStart:m.getViewPortEnd()], "\n")
	}
	return strings.Join(lines[m.viewPortStart:], "\n")
}

func (m Model) getViewPortEnd() int {
	return m.viewPortStart + m.GetTextHeight()
}

/*
*  render all the content of the list.
*
*  later, the viewport will be applied to this.
 */
func (m Model) renderContent() ([]string, int) {
	/*
	* render
	 */
	s := []string{}
	if m.width == 0 {
		// this will be callen before we get the initial screen size message
		// - so, just make sure we dont break by trying to render stuff
		//config.Logger.Warn().Msgf("screen too small for rendering! (%dx%d)", m.width, m.height)
		return s, 0
	}
	currentGroup := ""
	cursorLine := 0
	styles := []lipgloss.Style{config.Styles.PassiveItemStyle, config.Styles.AlternatePassiveStyle}
	styleIndex := 1
	items := m.GetItems()
	for idx, item := range items {
		hasCursor := false
		isActive := false
		if item.Group() != currentGroup {
			styleIndex += 1
			styleIndex %= len(styles)
			currentGroup = item.Group()
			s = append(s, m.renderGroup(currentGroup, styles[styleIndex]))
		}
		if idx == m.cursor {
			hasCursor = true
			cursorLine = len(s)
		}
		if idx == m.activeItem {
			isActive = true
		}
		s = append(s, m.renderItem(item, styles[styleIndex], hasCursor, isActive))
	}
	return s, cursorLine
}

func (m Model) View() string {
	if m.height > 0 {
		s, _ := m.renderContent()

		style := config.Styles.PassiveBorder.Inherit(m.baseStyle)
		if m.isActive {
			style = config.Styles.ActiveBorder.Inherit(m.baseStyle)
		}

		title := m.GetTitle()
		viewPort := m.drawViewPort(s)
		return m.RenderBox(title, viewPort, style)
		// return style.Render(m.drawViewPort(s))
	}
	return ""
}
