package list

import (
	tea "github.com/charmbracelet/bubbletea"
)

func (m *Model) HandleKeys(msg tea.KeyMsg) tea.Cmd {
	switch msg.String() {
	}
	return nil
}

func (m *Model) UpdateViewPort() {
	_, cursorLine := m.renderContent()
	m.cursorLine = cursorLine
	// if cursorline is above viewport (with a slight offset), move viewport up
	for m.cursorLine-3 <= m.viewPortStart && m.viewPortStart != 0 {
		m.viewPortStart -= 1
	}
	// if below, move viewport down
	for m.cursorLine+2 > m.getViewPortEnd() {
		m.viewPortStart += 1
	}
}

func (m Model) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	cmds := make([]tea.Cmd, 0)

	// Return the updated model to the Bubble Tea runtime for processing.
	// Note that we're not returning a command.
	return m, tea.Batch(cmds...)
}
