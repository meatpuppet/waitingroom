package list

import (
	"strings"

	"codeberg.org/meatpuppet/waitingroom/config"
	"github.com/charmbracelet/lipgloss"
)

// adapted from https://gist.github.com/meowgorithm/1777377a43373f563476a2bcb7d89306
// (see https://github.com/charmbracelet/lipgloss/pull/97)

func (m Model) RenderBox(title, content string, borderStyle lipgloss.Style) string {
	var (
		// Query the box style for some of its border properties so we can
		// essentially take the top border apart and put it around the label.
		border          lipgloss.Border     = borderStyle.GetBorderStyle()
		topBorderStyler func(... string) string = lipgloss.NewStyle().Foreground(borderStyle.GetBorderTopForeground()).Render
		topLeft         string              = topBorderStyler(border.TopLeft)
		topRight        string              = topBorderStyler(border.TopRight)
		
		bottomLeft         string              = topBorderStyler(border.BottomLeft)
		bottomRight        string              = topBorderStyler(border.BottomRight)

		duration = m.GetDuration()
		renderedLabel string =  config.Styles.HeadlineStyle.Render(title)
		renderedBottom string =  config.Styles.HeadlineStyle.Render(HumanizeTime(duration))
	)

	// Render top row with the label
	borderWidth := borderStyle.GetHorizontalBorderSize()
	cellsShort := max(0, m.width+borderWidth-lipgloss.Width(topLeft+topRight+renderedLabel))
	gap := strings.Repeat(border.Top, cellsShort)
	top := topLeft + renderedLabel + topBorderStyler(gap) + topRight
	
	bottom := ""
	if duration != 0 {
		cellsShort = max(0, m.width+borderWidth-lipgloss.Width(bottomLeft+bottomRight+renderedBottom))
		gap = strings.Repeat(border.Bottom, cellsShort)
		bottom = bottomLeft + renderedBottom + topBorderStyler(gap) + bottomRight
	} else {
		cellsShort = max(0, m.width+borderWidth-lipgloss.Width(bottomLeft+bottomRight))
		gap = strings.Repeat(border.Bottom, cellsShort)
		bottom = bottomLeft + topBorderStyler(gap) + bottomRight
	}

	// Render the rest of the box
	contentBlock := borderStyle.
		BorderTop(false).
		BorderBottom(false).
		Width(m.width).
		Render(content)

	// Stack the pieces
	return top  + "\n" + contentBlock + "\n" + bottom
}

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}
