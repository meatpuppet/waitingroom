package list

import "time"

type Item interface {
	Text() string
	TextRight() string
	Group() string
	Length() int

	Id() int
	GetPath() string
	GetLastModified() time.Time
}
