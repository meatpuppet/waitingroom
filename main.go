/*
Copyright © 2023 NAME HERE <EMAIL ADDRESS>
*/
package main

import "codeberg.org/meatpuppet/waitingroom/cmd"

func main() {
	cmd.Execute()
}
