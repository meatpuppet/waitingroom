package config

type GeneralKeys struct {
	Quit                 string `mapstructure:"quit" default:"q"`
	GoToBeginning        string `mapstructure:"go_to_beginning" default:"g"`
	GoToEnd              string `mapstructure:"go_to_end" default:"G"`
	GoToNextMatch        string `mapstructure:"go_to_next_match" default:"n"`
	Up                   string `mapstructure:"up" default:"up"`
	Down                 string `mapstructure:"down" default:"down"`
	MoveItemUp           string `mapstructure:"move_item_up" default:"<"`
	MoveItemDown         string `mapstructure:"move_item_down" default:">"`
	RemoveItem           string `mapstructure:"remove_item" default:"delete"`
	SwitchColumn         string `mapstructure:"switch_column" default:"tab"`
	Search               string `mapstructure:"search" default:"/"`
	Command              string `mapstructure:"command" default:":"`
	PlayNext             string `mapstructure:"play_next" default:"a"`
	Append               string `mapstructure:"append" default:"A"`
	PlayToggle           string `mapstructure:"play_toggle" default:" "`
	Enter                string `mapstructure:"enter" default:"enter"`
	FolderUp             string `mapstructure:"folder_up" default:"backspace"`
	UpdateLibrary        string `mapstructure:"update_library" default:"u"`
	TogglePlaylistColumn string `mapstructure:"toggle_playlist_column" default:"shift+tab"`

	MarkPlaylist string `mapstructure:"mark_playlist" default:"m"`

	// skip 10s
	FastForward string `mapstructure:"fast_forward" default:"right"`
	Rewind      string `mapstructure:"rewind" default:"left"`

	Yank  string `mapstructure:"yank" default:"y"`
	Paste string `mapstructure:"paste" default:"p"`

	VolumeUp   string `mapstructure:"volume_up" default:"+"`
	VolumeDown string `mapstructure:"volume_down" default:"-"`
}

type QueueKeys struct {
	Clear string `mapstructure:"clear" default:"c"`
	Play  string `mapstructure:"play" default:"enter"`
}

type Keys struct {
	General GeneralKeys `mapstructure:"general"`
	Queue   QueueKeys   `mapstructure:"queue"`
}
