package config

import (
	"github.com/rs/zerolog"
)

type Symbols struct {
	Playing string `mapstructure:"playing" default:">"`
	Stopped string `mapstructure:"stopped" default:"#"`
	Paused  string `mapstructure:"paused" default:":"`
}

type Configuration struct {
	LogPath  string `mapstructure:"log_path" default:""`
	LogLevel string `mapstructure:"log_level" default:"warn"`

	MpdHost     string  `mapstructure:"mpd_host" default:"127.0.0.1"`
	MpdPort     string  `mapstructure:"mpd_port" default:"6600"`
	MpdPassword string  `mapstructure:"mpd_password" default:""`
	Keys        Keys    `mapstructure:"keys"`
	Symbols     Symbols `mapstructure:"symbols"`
	Colors      Colors  `mapstructure:"colors"`

	MinColumnWidth int `mapstructure:"min_column_width" default:"90"`

	SkipSeconds int `mapstructure:"skip_seconds" default:"5"`
}

var Config Configuration
var Logger zerolog.Logger
