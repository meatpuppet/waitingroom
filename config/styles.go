package config

import (
	"github.com/charmbracelet/lipgloss"
)

type Colors struct {
	CursorBackground         string `mapstructure:"cursor_background" default:"#5e8d87"`
	CursorForeground         string `mapstructure:"cursor_foreground" default:"#d7d8d9"`
	InactiveCursorBackground string `mapstructure:"inactive_cursor_background" default:"#686868"`
	InactiveCursorForeground string `mapstructure:"inactive_cursor_foreground" default:"#b7b8b9"`

	Foreground          string `mapstructure:"foreground" default:"#b7b8b9"`
	ForegroundAlternate string `mapstructure:"foreground_alternate" default:"#a0a0a0"`
	ActiveBorder        string `mapstructure:"active_border" default:"#5e8d87"`
	PassiveBorder       string `mapstructure:"passive_border" default:"#a0a0a0"`
}

type styles struct {
	PassiveItemStyle        lipgloss.Style
	CursorItemStyle         lipgloss.Style
	InactiveCursorItemStyle lipgloss.Style
	AlternatePassiveStyle   lipgloss.Style
	ActiveItemStyle         lipgloss.Style
	BlockStyle              lipgloss.Style
	HeadlineStyle           lipgloss.Style

	BorderBase    lipgloss.Style
	ActiveBorder  lipgloss.Style
	PassiveBorder lipgloss.Style
}

var Styles styles
