package statusbar

import (
	"codeberg.org/meatpuppet/waitingroom/mpdclient"
	tea "github.com/charmbracelet/bubbletea"
	"time"
)

func (m *Model) SetSize(width, height int) {
	m.width = width
	m.height = height
}

type TickMsg time.Time

// command to update every second. see Init and case tickMsg in Update.
func TickCmd() tea.Cmd {
	return tea.Tick(time.Second, func(t time.Time) tea.Msg {
		return TickMsg(t)
	})
}

func (m Model) Update(msg tea.Msg) (Model, tea.Cmd) {
	cmds := make([]tea.Cmd, 0)

	switch msg := msg.(type) {
	case TickMsg:
		if m.State == mpdclient.PlayingState {
			m.elapsed += 1
		}
		cmds = append(cmds, TickCmd())
	case tea.WindowSizeMsg:
		m.SetSize(msg.Width, msg.Height)
	}

	return m, tea.Batch(cmds...)
}
