package statusbar

import (
	"codeberg.org/meatpuppet/waitingroom/mpdclient"
	tea "github.com/charmbracelet/bubbletea"
)

func (m *Model) Reset() {
	m.optionText = ""
}

func (m *Model) SetOptionText(text string) {
	m.optionText = text
}

func (m *Model) SetPlaying(track mpdclient.Track, elapsed int, state mpdclient.State, volume int) {
	m.track = track
	m.elapsed = elapsed
	m.State = state
	m.Volume = volume
}

type Model struct {
	height int
	width  int

	track   mpdclient.Track
	elapsed int
	State   mpdclient.State
	Volume  int

	// text on the right side
	optionText string
}

func (m Model) Init() tea.Cmd {
	return nil
}

func New() Model {
	return Model{
		height:  1,
		width:   1,
		elapsed: 0,
		State:   mpdclient.StoppedState,
	}
}
