package statusbar

import (
	"fmt"

	"codeberg.org/meatpuppet/waitingroom/config"
	"codeberg.org/meatpuppet/waitingroom/mpdclient"
)

func (m Model) getPlayingStr() string {
	return fmt.Sprintf("%s %s - %s [%s/%s]", m.State.GetStateSymbol(), m.track.Artist, m.track.Title, mpdclient.HumanizeTime(m.elapsed), mpdclient.HumanizeTime(m.track.Duration))
}

func (m Model) drawProgress() string {
	// chars dont necessarily have len of 1, so convert to rune array first, and work with that
	songInfoRunes := []rune(m.getPlayingStr())
	volumeMsgRunes := []rune(fmt.Sprintf("vol%d%%", m.Volume))

	songInfoSpace := m.width - len(volumeMsgRunes)

	if len(songInfoRunes) < songInfoSpace {
		// pad s with width whitespace runes to the right
		// * is for the width argument
		// - means pad to the right
		songInfoRunes = []rune(fmt.Sprintf("%-*s", songInfoSpace, string(songInfoRunes)))
	} else if songInfoSpace <= 1 {
		songInfoRunes = []rune{}
	} else {
		songInfoRunes = songInfoRunes[:songInfoSpace-1]
	}

	// build final status string
	statusRunes := append(songInfoRunes, volumeMsgRunes...)

	if m.elapsed == 0 {
		return string(statusRunes)
	}

	songProgress := m.elapsed * 100 / m.track.Duration
	progressBar := songProgress * m.width / 100
	if progressBar > len(statusRunes) {
		config.Logger.Error().Msgf("progressbar is %d long, but statusRunes is only %d", progressBar, len(songInfoRunes))
		progressBar = len(statusRunes)
	}
	progressed := string(statusRunes[:progressBar])
	unprogressed := string(statusRunes[progressBar:])
	return config.Styles.CursorItemStyle.Render(progressed) + config.Styles.PassiveItemStyle.Render(unprogressed)
}

func (m Model) getInfoStr() string {
	if m.optionText == "" {
		return m.getPlayingStr()
	}
	return m.getPlayingStr() + " - " + m.optionText
}

func (m Model) View() string {
	if m.height < 1 {
		// dont return anything if we dont have a height yet
		return ""
	}
	return m.drawProgress()
}
